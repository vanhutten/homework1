﻿using System.Collections.Generic;
using Otus.Teaching.Linq.ATM.Core.Entities;
using System.Linq;

namespace Otus.Teaching.Linq.ATM.Core.Services
{
    public class ATMManager
    {
        public IEnumerable<Account> Accounts { get; private set; }
        
        public IEnumerable<User> Users { get; private set; }
        
        public IEnumerable<OperationsHistory> History { get; private set; }
        
        public ATMManager(IEnumerable<Account> accounts, IEnumerable<User> users, IEnumerable<OperationsHistory> history)
        {
            Accounts = accounts;
            Users = users;
            History = history;
        }
        
        //TODO: Добавить методы получения данных для банкомата
        public User GetUser(string login, string passwd)
        {
            return Users.Where(x => x.Login == login).Where(x => x.Password == passwd).FirstOrDefault();
        }

        public IEnumerable<Account> GetUserAccounts(string login)
        {
            if(login != null)
            {
                return Accounts.Where(x => x.UserId == Users.Where(x => x.Login == login).Select(x => x.Id).FirstOrDefault());
            }
            else return null;
        }

        public IEnumerable<OperationsHistory> GetOperationHistory(IEnumerable<int> AccountID)
        {
            if(AccountID != null)
            {
                List<OperationsHistory> list = new List<OperationsHistory>();
                foreach(int id in AccountID)
                {
                    var tmp = History.Where(x => x.AccountId == id).ToList();
                    if(tmp != null)
                        list.AddRange(tmp);
                }
                return list;
               
            }
            else
                return null;
        }

        public string GetInputCashOperations()
        {
            string s = string.Empty;
            var query = History.Where(x => x.OperationType == OperationType.InputCash)
                .Select(o => Users.Where(x => x.Id == o.AccountId).Select(y => new { Query = string.Concat("ID= " + o.AccountId.ToString(),
                 " Date - " + o.OperationDate.ToString(), " Cash = " + o.CashSum.ToString(), " Name = " + y.FirstName.ToString(), " "
                 + y.MiddleName.ToString(), " " + y.SurName.ToString()+"\n") }).Select(g=>g.Query));
            foreach(var item in query)
            {
                s+=string.Join(',', item);
            }
            return s;                      
        }

        public string GetUsersHoldingMoreThan(decimal Cash)
        {
            string s = string.Empty;
            var str = Users.GroupBy(g => g.Id).Select(u => u.Select(id => Accounts.Where(ac => ac.UserId == id.Id).Select(a => a.CashAll).Sum()).Where(x => x > Cash)
              .Select(cash => new 
              { 
                  query = string.Concat("ID = "+ u.Select(id=>id.Id).FirstOrDefault().ToString(),
                  " Имя: ", u.Select(n=> new { FullName = string.Concat(n.FirstName," ", n.MiddleName, " ", n.SurName) }).Select(name => name.FullName).FirstOrDefault(),
                  " Сумма = " + cash.ToString()+ "\n")
              }
              ).Select(q=>q.query));

            foreach(var item in str)
            {
                s += string.Concat(item);
            }
            return s;
        }
    }
}