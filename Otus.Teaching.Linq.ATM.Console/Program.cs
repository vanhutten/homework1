﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using Otus.Teaching.Linq.ATM.Core.Services;
using Otus.Teaching.Linq.ATM.DataAccess;
using Otus.Teaching.Linq.ATM.Core.Entities;

namespace Otus.Teaching.Linq.ATM.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.WriteLine("Старт приложения-банкомата...");

            var atmManager = CreateATMManager();

            //TODO: Далее выводим результаты разработанных LINQ запросов
            //1. вывод информации по заданному логину и паролю
            System.Console.WriteLine("Задание 1");
            System.Console.WriteLine("Введите логин");
             string login = System.Console.ReadLine();
            System.Console.WriteLine("Введите пароль");
            string pass = System.Console.ReadLine();
            var user = atmManager.GetUser(login, pass);
            if(user != null)
                System.Console.WriteLine("Имя = {0}; Фамилия = {1}; Отчество = {2}",user.FirstName,user.SurName, user.MiddleName);
            else
                System.Console.WriteLine("Данного пользователя не существует");
            
            //2. Вывод данных о всех счетах заданного пользователя
            System.Console.WriteLine("Задание 2. определение данных о счетах пользователя");
            System.Console.WriteLine("Введите логин");
            login = System.Console.ReadLine();
            var accountIDs = atmManager.GetUserAccounts(login).Select(x => x.Id);
            System.Console.WriteLine("ID счетов: "+string.Join(',', accountIDs));
            
            //3. Вывод данных о всех счетах заданного пользователя
            System.Console.WriteLine("Задание 3. вывод истории по каждому счету из задания 2");
            string output = string.Join(',', atmManager.GetOperationHistory(accountIDs).Select(x => new { x.Id, x.OperationDate, x.CashSum }));
            System.Console.WriteLine(output);
            //4. Вывод данных о всех операциях пополнения счёта с указанием владельца каждого счёта;
            System.Console.WriteLine("Задание 4. Вывод данных о всех операциях пополнения счёта с указанием владельца каждого счёта");
            System.Console.WriteLine(atmManager.GetInputCashOperations());
            //5.Вывод данных о всех пользователях у которых на счёте сумма больше N(N задаётся из вне и может быть любой);
            System.Console.WriteLine("Задание 5.Вывод данных о всех пользователях у которых на счёте сумма больше N(N задаётся из вне и может быть любой)");
            System.Console.WriteLine("Введите сумму");
            output = string.Empty;
            string input = System.Console.ReadLine();
            if(decimal.TryParse(input, out decimal cash))
            {
                System.Console.WriteLine(atmManager.GetUsersHoldingMoreThan(cash));
            }

            System.Console.WriteLine("Завершение работы приложения-банкомата...");


        }

        static ATMManager CreateATMManager()
        {
            using var dataContext = new ATMDataContext();
            var users = dataContext.Users.ToList();
            var accounts = dataContext.Accounts.ToList();
            var history = dataContext.History.ToList();
                
            return new ATMManager(accounts, users, history);
        }

    }
}